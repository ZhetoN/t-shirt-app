<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
	protected $fillable = ['media_id', 'x', 'y', 'width', 'height', 'scale'];

	public $timestamps = false;

    public function media()
    {
        return $this->belongsTo('App\Models\Media');
    }
}
