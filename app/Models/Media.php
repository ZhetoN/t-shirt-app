<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{
    protected $fillable = ['mime', 'filename', 'size', 'path', 'width', 'height'];

    public function product()
    {
        return $this->hasOne('App\Models\Product');
    }
}
