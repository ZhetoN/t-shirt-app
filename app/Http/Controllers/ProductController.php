<?php

namespace App\Http\Controllers;

use \Input;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Media as MediaModel;
use App\Models\Product as ProductModel;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    const PRINT_WIDTH = 450;
    const PRINT_HEIGHT = 525;
    const CANVAS_WIDTH = 222;
    const CANVAS_HEIGHT = 259;
    const THUMB_WIDTH = 222;
    const THUMB_HEIGHT = 259;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductModel::All();

        return \View::make('orders')->with('products', $products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        $scale = $data['scale'];
        $k = self::THUMB_WIDTH / self::CANVAS_WIDTH;
        $scale *= $k;

        $imagedata = (object) array();
        $imagedata->x = round($data['x'] * $k);
        $imagedata->y = round($data['y'] * $k);
        $imagedata->width = round($data['width'] * $scale);
        $imagedata->height = round($data['height'] * $scale);
        $imagedata->scale = $scale;

        $model = MediaModel::find($data['id']);

        $directory = public_path() . '/uploads/';

        Image::configure(array('driver' => 'imagick'));
        $canvas = Image::canvas(self::THUMB_WIDTH, self::THUMB_HEIGHT, '#ffffff');

        $img = Image::make($directory . $model->path)
             ->resize($imagedata->width, $imagedata->height);

        $canvas->insert($img, 'top-left', $imagedata->x, $imagedata->y);

        $product = ProductModel::create([
            'media_id' => $model->id,
            'x' => $data['x'],
            'y' => $data['y'],
            'width' => $data['width'],
            'height' => $data['height'],
            'scale' => $data['scale']
        ]);

        $canvas->save(public_path() . '/orders/' . $product->id . '_' . $model->path);

        return response()->json($imagedata, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $directory = public_path() . '/uploads/';
        $product = ProductModel::find($id);
        $media = $product->media;

        $k = self::PRINT_WIDTH / self::CANVAS_WIDTH;

        $imagedata = (object) array();
        $imagedata->x = round($product->x * $k);
        $imagedata->y = round($product->y * $k);
        $imagedata->width = round($product->width * $product->scale * $k);
        $imagedata->height = round($product->height * $product->scale * $k);
        $imagedata->scale = $product->scale;

        Image::configure(array('driver' => 'imagick'));
        $canvas = Image::canvas(self::PRINT_WIDTH, self::PRINT_HEIGHT, '#ffffff');
        $img = Image::make($directory . $media->path)
             ->resize($imagedata->width, $imagedata->height);

        $canvas->insert($img, 'top-left', $imagedata->x, $imagedata->y);

        return $canvas->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
