<?php

namespace App\Http\Controllers;

use \Storage;
use \File;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Media as MediaModel;

use Flow\Config as FlowConfig;
use Flow\Request as FlowRequest;
use Flow\File as FlowFile;
use Flow\ConfigInterface;
use Flow\RequestInterface;

use Intervention\Image\ImageManagerStatic as Image;

class MediaController extends Controller
{
    /**
     * Handle uploads.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload()
    {
        $config = new FlowConfig();
        $temp = storage_path() . '/tmp';
        $config->setTempDir($temp);

        $file = new FlowFile($config);
        $request = new FlowRequest();

        $totalSize = $request->getTotalSize();

        if ($totalSize && $totalSize > (1024 * 1024 * 20)) {
            return \Response::make('File size exceeds 20MB', 400);
        }

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            if (!$file->checkChunk() ) {
                return \Response::make('', 204);
            }
        } else {
            if ($file->validateChunk()) {
                $file->saveChunk();
            } else {
                return \Response::make('', 400);
            }
        }

        $destination = public_path() . '/uploads/';
        $extension = File::extension($request->getFileName());
        $filename = sha1(time() . time()) . ".{$extension}";

        if ($file->validateFile() && $file->save($destination . $filename)) {

            Image::configure(array('driver' => 'imagick'));
            $img = Image::make($destination . $filename);
            $width = $img->width();
            $height = $img->height();

            $file = MediaModel::create([
                'mime' => File::mimeType($destination . $filename),
                'filename' => $request->getFileName(),
                'size' => $totalSize,
                'path' => $filename,
                'width' => $width,
                'height' => $height,
            ]);

            $response = (object) array();
            $response->id = $file->id;
            $response->width = $width;
            $response->height = $height;
            return response()->json($response, 200);
        }

    }

}
