var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var elixir = require('laravel-elixir');



elixir(function(mix) {
	var assets_path = "./resources/assets/vendor/";

	var options = {
			includePaths: [
				assets_path + "seiyria-bootstrap-slider/sass/"
			]
		}

	var bpath = 'node_modules/bootstrap-sass/assets',
		jquery_path =  assets_path + 'jquery',
		angular_path = assets_path + 'angular',
		ngflow_path = assets_path + 'ng-flow',
		animate_path = assets_path + 'angular-animate',
		slider_path = assets_path + 'seiyria-bootstrap-slider',
		ngslider_path = assets_path + 'angular-bootstrap-slider',
		toggle_path = assets_path + 'bootstrap-toggle';


	mix.sass('app.scss')
		.styles([
			slider_path + '/dist/css/bootstrap-slider.min.css',
			'./resources/assets/css/whirly.css',
			toggle_path + '/css/bootstrap-toggle.min.css',
			'./public/css/app.css'
		])
		.copy(jquery_path + '/dist/jquery.min.js', 'public/js')
		.copy(angular_path + '/angular.min.js', 'public/js')
		.copy(ngflow_path + '/dist/ng-flow-standalone.min.js', 'public/js')
		.copy(bpath + '/fonts', 'public/fonts')
		.copy(bpath + '/javascripts/bootstrap.min.js', 'public/js')
		.copy(animate_path + '/angular-animate.min.js', 'public/js')
		.copy(slider_path + '/dist/bootstrap-slider.min.js', 'public/js')
		.copy(ngslider_path + '/slider.js', 'public/js')
		.copy(toggle_path + '/js/bootstrap-toggle.min.js', 'public/js')
	});




