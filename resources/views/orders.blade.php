@extends('index')

<div class="col-md-offset-2 col-md-8">
<div class="row">
@foreach ($products as $product)
    <div class="col-lg-2 col-sm-3 col-xs-4">
        <a href="/order/{{ $product->id }}" target="_blank">
			<img src="/orders/{{ $product->id }}_{{ $product->media->path }}"
				 alt="order {{ $product->id }}" class="img-thumbnail">
		</a>
    </div>
@endforeach
</div>
</div>