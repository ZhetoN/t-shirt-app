@extends('index')
<div class="col-md-offset-3 col-md-6" flow-drop
	 flow-drop-enabled="true"
	 flow-drag-enter="style={outline:'1px solid lightblue'}"
	 flow-drag-leave="style={}"
		 ng-style="style"
		 ng-controller="canvasCtrl">
	<div class="progress progress-striped upload-progress"
		 ng-hide="!$flow.isUploading()">
	    <div class="progress-bar"
	    	 role="progressbar"
	    	 ng-style="{'width' : ($flow.progress() * 100) + '%'}">
	   	</div>
	</div>
	<div class="text-center whirly-loader-wrap" ng-hide="!image.processing">
		<div class="whirly-loader">Loading...</div>
	</div>
	<div ng-controller="canvasCtrl"
		 html-canvas src="$flow.files[$flow.files.length - 1]"
		 background="{{asset('assets/images/shirt.png')}}">
		 <div class="canvas-widget"></div>
		<div class="buttons-wrap col-md-offset-1 col-md-10">
			<span class="btn btn-default" flow-btn>Upload Image</span>
			<label ng-click="setCover()" class="checkbox-inline">Cover<input type="checkbox" data-toggle="toggle"></label>
			<span class="btn btn-default" ng-click="fitToCanvas()">Fit</span>
			<span class="btn btn-primary pull-right" ng-click="saveImageData()">Order</span>
		</div>
	</div>
	<div class="scale-slider col-md-offset-1 col-md-10 ng-animate"
		 ng-controller="ScaleCtrl"
		 ng-hide="image.minScale >= image.maxScale || $flow.isUploading()">
		<slider ng-model="image.scale"
				min="image.minScale"
				step="scaleOptions.step"
				max="image.maxScale"
				value="image.scale"
				tooltip="hide">
		</slider>
	</div>
</div>