<!DOCTYPE html>
<html ng-app="app"
	  flow-init="{target: '/upload'}"
	  flow-files-submitted="$flow.upload()"
	  flow-file-success="$file.msg = $message"
	>
    <head>
        <title>T-Shirt sample App</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        {!! Html::style('css/all.css') !!}
    </head>

    <body flow-prevent-drop>
	    <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container">
	            <div class="navbar-header">
	              <button type="button" class="navbar-toggle collapsed"
	              		  data-toggle="collapse" data-target="#navbar"
	              		  aria-expanded="false" aria-controls="navbar">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="#">T-Shirt sample App</a>
	            </div>
	            <div id="navbar" class="collapse navbar-collapse">
	                <ul class="nav navbar-nav">
	                    <li class="{{ Request::is('/') ? 'active' : '' }}">
	                    	<a href="{{ URL::to('/') }}">Create Design</a>
	                    </li>
	                    <li class="{{ Request::is('admin') ? 'active' : '' }}">
	                    	<a href="{{ URL::to('admin') }}">Orders</a>
	                    </li>
	                </ul>
	            </div><!--/.nav-collapse -->
	        </div>
	    </nav>

        <div class="container">

            @yield('content')
        </div><!-- /.container -->
		{!! Html::script('js/jquery.min.js') !!}
        {!! Html::script('js/bootstrap.min.js') !!}
        {!! Html::script('js/bootstrap.min.js') !!}
        {!! Html::script('js/angular.min.js') !!}
        {!! Html::script('js/ng-flow-standalone.min.js') !!}
        {!! Html::script('js/angular-animate.min.js') !!}
        {!! Html::script('js/bootstrap-slider.min.js') !!}
        {!! Html::script('js/slider.js') !!}
        {!! Html::script('js/bootstrap-toggle.min.js') !!}
        {!! Html::script('js/app.js') !!}
    </body>
</html>