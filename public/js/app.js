/*global angular */
'use strict';

/**
 * The main app module
 * @name app
 * @type {angular.Module}
 */
var app = angular.module('app', ['flow', 'ngAnimate', 'ui.bootstrap-slider']);

app.config(function (flowFactoryProvider) {
        flowFactoryProvider.defaults = {
            target: 'upload',
            permanentErrors: [404, 415, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 4,
            allowDuplicateUploads: true,
            headers: function (file, chunk, isTest) {
                return {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }
        };

    // flowFactoryProvider.on('catchAll', function (event) {
    //     console.log('catchAll', arguments);
    // });

});

app.controller("canvasCtrl", ['$scope', function($scope) {
    $scope.image = {
        scale: 0,
        minScale: 0,
        maxScale: 0,
        processing: false
    }
}]);

app.directive('htmlCanvas',
    ['$document', 'orderService', function($document, orderService) {

    return {
        restrict : 'A',
        $scope: {
            eventHandler: '&ngClick'
        },
        link: function(scope, element, attr) {

            element.addClass('canvas-widget');

            var wrapElm = element.find('.canvas-widget'),
                canvas = $('<canvas>')
                       .addClass('html-canvas')
                       .appendTo(wrapElm);

            // canvas.css("background-color", "lightgray");

            var img = $('<img>', {
                src: attr.background,
                'class': 'canvas-widget-background'
            }).on('dragstart', function(ev){
                ev.preventDefault();
            }).appendTo(wrapElm);

            var offset = {
                    x: 0,
                    y: 0
                },
                htmlCanvas = canvas.get(0),
                ctx = htmlCanvas.getContext('2d'),
                bounds = null,
                fileReader = new FileReader(),
                // object for handling uploaded image and all properties
                fileImage = {
                    id: null,
                    imgObj: new Image(),
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0,
                    srcWidth: 0,
                    srcHeight: 0,
                    minScale: 0,
                    maxScale: 0,
                    scale: 100
                },
                // check for browser's css pointer-event support
                pointerEvents = 'pointerEvents' in element.get(0).style,
                currentCursor = 'default',
                // if true minimal scale is set to cover printing area,
                cover = false,
                // if cover false use minimal print size in mm
                printSize = {
                    width: 120,
                    height: 120
                },
                // This for setting maximal scale level
                // preventing customer to scale image too much
                // assume CMYK printing on textile LPI = 60
                // DPI (for cmyk images) = LPI x 1.5
                minDPI = 90,
                // print area size, 300 x 350 mm
                // 4500 x 5250 px @381dpi
                printArea = {
                    width: 300,
                    height: 350
                },
                mmPerInch = 25.4
            ;

            htmlCanvas.width = canvas.width();
            htmlCanvas.height = canvas.height();

            fileImage.imgObj.onload = function() {
                // hide progress indicator
                scope.$apply(function() {
                    scope.$parent.image.processing = false;
                });
                fitAndCenter(true);
                $(htmlCanvas).css('cursor', 'move');
            }

            function isHit(x, y, b) {
                if (x > b.left && x < b.right && y > b.top && y < b.bottom) {
                    return true;
                }
                return false;
            }

            function getImageData(file) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function (event) {
                    $(fileImage.imgObj).attr('src', event.target.result);
                }
            }

            function normalizeScale(value) {
                var min = fileImage.minScale,
                    max = fileImage.maxScale,
                    normalized = (value - min) / (max - min),
                    scale = normalized * (max - 1) + 1;
                return scale;
            }

            function drawImage() {
                ctx.clearRect(0, 0, htmlCanvas.width, htmlCanvas.height);
                var scale = fileImage.scale;
                if (cover) {
                    scale = normalizeScale(scale);
                }
                ctx.save();
                ctx.scale(scale, scale);
                ctx.drawImage(
                    fileImage.imgObj,
                    fileImage.x / scale,
                    fileImage.y / scale,
                    fileImage.width,
                    fileImage.height
                );
                ctx.restore();
            }

            function normalizeImage() {
                var scale = fileImage.scale;
                if (cover) {
                    scale = normalizeScale(scale);
                }
                var minX = htmlCanvas.width - fileImage.width * scale,
                    minY = htmlCanvas.height - fileImage.height * scale,
                    maxX = 0,
                    maxY = 0;

                if (!cover) {
                    var w = fileImage.width * scale,
                        h = fileImage.height * scale;

                    if (htmlCanvas.width > w) {
                        minX = 0;
                        maxX = htmlCanvas.width - w;
                    }

                    if (htmlCanvas.height > h) {
                        minY = 0;
                        maxY = htmlCanvas.height - h
                    }
                }

                fileImage.x = Math.max(minX, Math.min(fileImage.x, maxX));
                fileImage.y = Math.max(minY, Math.min(fileImage.y, maxY));
            }


            scope.saveImageData = function() {
                if (!fileImage.imgObj.src) {
                    return;
                }
                var scale = fileImage.scale;
                if (cover) {
                    scale = normalizeScale(scale);
                }

                var data = {
                    id: fileImage.id,
                    x: fileImage.x,
                    y: fileImage.y,
                    width: fileImage.width,
                    height: fileImage.height,
                    scale: scale
                };

                orderService.addOrder(data).then(
                    function(message) {
                        console.warn(message);
                    }
                );
            }


            scope.setCover = function() {
                cover = !cover;
                updateScaleRange();
                scope.$parent.image.minScale = fileImage.minScale * 100;
                scope.$parent.image.maxScale = fileImage.maxScale * 100;
                scope.$parent.image.scale = fileImage.scale * 100;
                fitAndCenter();
                scope.$parent.image.scale = fileImage.scale * 100;
            }

            scope.fitToCanvas = function() {
                fitAndCenter();
                scope.$parent.image.scale = fileImage.scale * 100;
            }

            scope.$parent.$watch('image.scale', function() {
                // keep image scale origin at canvas center
                var origin = {
                        x: htmlCanvas.width / 2,
                        y: htmlCanvas.height / 2
                    },
                    // distance from image top-left to origin without scale
                    dx = (origin.x - fileImage.x),
                    dy = (origin.y - fileImage.y),
                    scale = fileImage.scale,
                    newScale = scope.$parent.image.scale / 100;

                if (cover) {
                    scale = normalizeScale(scale);
                    newScale = normalizeScale(newScale);
                }

                // get distances difference
                // at current scale and at new scale
                var deltaX = dx * scale - dx * newScale,
                    deltaY = dy * scale - dy * newScale;

                // back to pixels @ new scale
                fileImage.x += deltaX / newScale;
                fileImage.y += deltaY / newScale;

                fileImage.scale = scope.$parent.image.scale / 100;
                normalizeImage();
                requestAnimationFrame(drawImage);
            });

            scope.$on(
                'flow::filesSubmitted',
                function (event, flowObj, flowFile, message) {
                    ctx.clearRect(0, 0, htmlCanvas.width, htmlCanvas.height);
                }
            );

            scope.$on(
                'flow::fileSuccess',
                function (event, flowObj, flowFile, message) {
                    // show progress indicator
                    scope.$apply(function() {
                        scope.$parent.image.processing = true;
                    });

                    // Better to reload image from server
                    // because of some RGB colors can't be printed in CMYK
                    // and customers will not see the real colors.
                    // First convert image to jpg (no CMYK supported by png)
                    // because we not sure uploaded image have any embedded
                    // profiles convert it to sRGB or AdobeRGB profile
                    // (AdobeRGB have wider color coverage
                    // than sRGB in green-blue areas)
                    // then convert to CMYK profile (e.g. EuroscaleCoated.icc)
                    // and then back to RGB.
                    // This will give as an image
                    // what looks like printed in real cmyk colors
                    getImageData(flowFile.file);

                    // ng-flow doesn't support allowDuplicateUploads
                    // supported by original flow.js
                    // just clean uploaded files
                    flowObj.files = [];

                    var data = JSON.parse(message);

                    fileImage.imgObj.src = '';
                    fileImage.srcWidth = data.width;
                    fileImage.srcHeight = data.height;
                    fileImage.id = data.id;

                    updateScaleRange(true);
                }
            );

            // calculate minimal and maximal scale, %
            // based on printing size and image DPI
            function updateScaleRange(updateScope) {
                // maximal size in mm
                var width = fileImage.srcWidth / minDPI * mmPerInch,
                    height = fileImage.srcHeight / minDPI * mmPerInch,
                    maxSX = width / printArea.width,
                    maxSY = height / printArea.height;
                fileImage.maxScale = Math.max(maxSX, maxSY);

                if (cover) {
                    var minSX = printArea.width / width,
                        minSY = printArea.height / height;
                    fileImage.minScale = Math.max(minSY, minSX);
                    fileImage.maxScale = Math.min(maxSX, maxSY);
                    fileImage.scale = fileImage.minScale;
                } else {
                    if (width > height) {
                        fileImage.minScale = printSize.width / printArea.width;
                    } else {
                        fileImage.minScale = printSize.height / printArea.height;
                    }
                    fileImage.maxScale = Math.max(maxSX, maxSY);
                    fileImage.scale = Math.max(
                        fileImage.minScale,
                        Math.min(fileImage.scale, fileImage.maxScale)
                    );
                }

                if (updateScope) {
                    // update scale slider
                    scope.$apply(function() {
                        scope.$parent.image.minScale = fileImage.minScale * 100;
                        scope.$parent.image.maxScale = fileImage.maxScale * 100;
                        scope.$parent.image.scale = fileImage.scale * 100;
                    });
                }
            }

            function fitAndCenter(updateScope) {
                var minW = htmlCanvas.width,
                    minH = htmlCanvas.height,
                    imgW = fileImage.srcWidth,
                    imgH = fileImage.srcHeight,
                    width = minW,
                    height = minH,
                    ratio  = htmlCanvas.width / htmlCanvas.height,
                    imgRation = imgW / imgH,
                    fit2w = ratio > imgRation;

                if (cover) {
                    if (fit2w) {
                        height = minW * imgH / imgW;
                        fileImage.x = 0;
                        fileImage.y = (minH - height) / 2;
                    } else {
                        width = minH * imgW / imgH;
                        fileImage.x = (minW - width) / 2;
                        fileImage.y = 0;
                    }
                    fileImage.scale = fileImage.minScale;
                } else {
                    minW = Math.min(minW, fileImage.srcWidth);
                    minH = Math.min(minH, fileImage.srcHeight);
                    if (fit2w) {
                        height = minH;
                        width = minH * imgW / imgH;
                    } else {
                        width = minW;
                        height = minW * imgH / imgW;
                    }

                    var scale = Math.min(fileImage.maxScale, 1);
                    fileImage.scale = scale;

                    fileImage.x = (htmlCanvas.width - width * scale) / 2;
                    fileImage.y = (htmlCanvas.height - height * scale) / 2;
                }

                fileImage.width = width;
                fileImage.height = height;

                if (updateScope) {
                    scope.$apply(function() {
                        scope.$parent.image.scale = fileImage.scale * 100;
                    });
                }
                drawImage();
            }

            // mouseover handler
            // only for browsers not supporting css pointer-events
            function overHandler(ev) {
                if (pointerEvents || !fileImage.imgObj.src) {
                    return;
                }
                var cursor = isHit(ev.pageX, ev.pageY, bounds)
                           ? 'move' : 'default';
                if (cursor != currentCursor) {
                    element.css('cursor', cursor);
                    currentCursor = cursor;
                }
            }

            function downHandler(ev) {
                ev.preventDefault();
                ev.stopPropagation();

                // use css pointer-events: none (IE 11+) on t-shirt image
                // and add mousedown event directly to html canvas
                // if pointer-events not supported
                // hit test for canvas element
                if (!pointerEvents && !isHit(ev.pageX, ev.pageY, bounds)) {
                    return;
                }

                offset = {
                    x: ev.localX - fileImage.x,
                    y: ev.localY - fileImage.y
                };

                $(document).on('mousemove', moveHandler);
                $(document).one('mouseup', upHandler);
                if (!pointerEvents) {
                    $(element).off('mousemove', overHandler);
                }
            }

            function moveHandler(ev) {
                ev.preventDefault();
                ev.stopPropagation();
                fileImage.x = ev.localX - offset.x;
                fileImage.y = ev.localY - offset.y;
                normalizeImage();
                requestAnimationFrame(drawImage);
            }

            function upHandler(ev) {
                ev.preventDefault();
                ev.stopPropagation();
                $(document).off('mousemove', moveHandler);
                if (!pointerEvents) {
                    $(element).on('mousemove', overHandler);
                }
            }

            $(pointerEvents ? htmlCanvas : element).on('mousedown', downHandler);
            if (!pointerEvents) {
                $(element).on('mousemove', overHandler);
            }


            // extend jQuery event:
            // add local html canvas event coordinates
            (function($) {
                'use strict';
                var events = ['mousedown', 'mousemove', 'mouseup'];
                var fix = {
                    filter: function(event, original) {
                        event.pageX = original.pageX;
                        event.pageY = original.pageY;
                        event.localX = original.pageX - $(window).scrollLeft();
                        event.localY = original.pageY - $(window).scrollTop();
                        if (htmlCanvas) {
                            bounds = htmlCanvas.getBoundingClientRect();
                            event.localX -= bounds.left;
                            event.localY -= bounds.top;
                        }
                        return event;
                    }
                };

                var i = 0;
                var length = events.length;

                for (; i < length; i++) {
                    $.event.fixHooks[events[i]] = fix;
                }
            }($));
        }
    };
}]);

app.service(
    "orderService",
    function($http, $q) {

        return({
            addOrder: addOrder
        });

        function addOrder(data) {
            var request = $http({
                method: "post",
                url: "order/create",
                data: data
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError( response ) {
            if (!angular.isObject( response.data )
                || !response.data.message) {
                return( $q.reject( "An unknown error occurred." ) );
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
);

app.controller('ScaleCtrl', ['$scope', function ($scope) {

        $scope.scaleOptions = {
            min: 0,
            max: 0,
            step: 0.01,
            precision: 2,
            orientation: 'horizontal',
            handle: 'round',
            tooltip: 'hide',
            tooltipseparator: ':',
            tooltipsplit: false,
            enabled: true,
            naturalarrowkeys: false,
            range: false,
            ngDisabled: false,
            reversed: false
        };
    }]
);
